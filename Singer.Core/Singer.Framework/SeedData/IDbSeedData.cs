﻿namespace Singer.Framework.SeedData;

/// <summary>
/// 种子数据初始化执行方法 接口
/// </summary>
public interface IDbSeedData
{
    /// <summary>
    /// 执行种子数据初始化方法
    /// </summary>
    Task ExecuteAsync();
}
