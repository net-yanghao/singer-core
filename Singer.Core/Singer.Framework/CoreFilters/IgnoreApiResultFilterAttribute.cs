﻿namespace Singer.Framework.CoreFilters;

/// <summary>
/// 忽略Api统一结果处理过滤器
/// </summary>
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public class IgnoreApiResultFilterAttribute : Attribute
{

}
