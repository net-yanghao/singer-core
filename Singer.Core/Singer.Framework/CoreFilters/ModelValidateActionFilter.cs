﻿using Microsoft.AspNetCore.Mvc.Filters;
using Singer.Shared;

namespace Singer.Framework.CoreFilters;

/// <summary>
/// 接口参数校验过滤器
/// </summary>
public class ModelValidateActionFilter : ActionFilterAttribute
{
    public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    {
        if (!context.ModelState.IsValid)
        {
            var errors = context.ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage);
            var error = string.Join(" | ", errors);
            throw new CoreException("接口参数异常：" + error, ErrorCode.Parameter);
        }
        await next.Invoke();
    }
}
