﻿using Microsoft.Extensions.DependencyInjection;

namespace Singer.Framework.CoreDI;

/// <summary>
/// 特性 将服务注入到DI中 
/// </summary>
[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
public class CoreDIAttribute : Attribute
{
    /// <summary>
    /// 要注入的接口类型
    /// </summary>
    public Type? InterfaceType { get; private set; }
    /// <summary>
    /// 服务的生命周期
    /// </summary>
    public ServiceLifetime ServiceLifetime { get; private set; }

    public CoreDIAttribute(Type? interfaceType = null, ServiceLifetime serviceLifeTime = ServiceLifetime.Scoped)
    {
        if (!Enum.IsDefined(serviceLifeTime))
            throw new Exception($"【CoreDI】 The Enum '{nameof(ServiceLifetime)}' value error.");
        InterfaceType = interfaceType;
        ServiceLifetime = serviceLifeTime;
    }
}
