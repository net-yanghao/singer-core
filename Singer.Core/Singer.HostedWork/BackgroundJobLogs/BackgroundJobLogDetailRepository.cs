﻿using Singer.Middleware.FreeSql;
using Singer.Shared.UserContext;
using System.Linq.Expressions;

namespace Singer.BackgroundJob.RabbitMQ.BackgroundJobLogs;

/// <summary>
/// 后台任务日志详情表 - 仓储
/// </summary>
public class BackgroundJobLogDetailRepository : BaseRepository<BackgroundJobLogDetail>, IBackgroundJobLogDetailRepository
{
    public BackgroundJobLogDetailRepository(IFreeSql fsql, IUserContextAccessor userContextAccessor, Expression<Func<BackgroundJobLogDetail, bool>>? filter = null, Func<string, string>? asTable = null) : base(fsql, userContextAccessor, filter, asTable)
    {
    }
}
