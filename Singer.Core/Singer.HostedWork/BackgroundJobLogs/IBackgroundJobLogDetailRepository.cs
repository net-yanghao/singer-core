﻿using Singer.Middleware.FreeSql;

namespace Singer.BackgroundJob.RabbitMQ.BackgroundJobLogs;

/// <summary>
/// 后台任务日志详情表 - 仓储接口
/// </summary>
public interface IBackgroundJobLogDetailRepository : IBaseRepository<BackgroundJobLogDetail>
{
}
