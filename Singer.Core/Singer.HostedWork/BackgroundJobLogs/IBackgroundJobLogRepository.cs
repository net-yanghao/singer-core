﻿using Singer.Middleware.FreeSql;

namespace Singer.BackgroundJob.RabbitMQ.BackgroundJobLogs;

/// <summary>
/// 后台任务日志 - 仓储接口
/// </summary>
public interface IBackgroundJobLogRepository : IBaseRepository<BackgroundJobLog>
{

}
