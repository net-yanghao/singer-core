﻿using Singer.Middleware.FreeSql;
using Singer.Shared.UserContext;

namespace Singer.BackgroundJob.RabbitMQ.BackgroundJobLogs;

/// <summary>
/// 后台任务日志 - 仓储
/// </summary>
public class BackgroundJobLogRepository : BaseRepository<BackgroundJobLog>, IBackgroundJobLogRepository
{
    public BackgroundJobLogRepository(IFreeSql fsql, IUserContextAccessor userContextAccessor) : base(fsql, userContextAccessor)
    {
    }
}
