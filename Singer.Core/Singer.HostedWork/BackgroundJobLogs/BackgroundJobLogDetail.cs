﻿using FreeSql.DataAnnotations;
using Singer.BackgroundJob.RabbitMQ.BackgroundJobLogs.Dtos;
using Singer.BackgroundJob.RabbitMQ.Contracts.BackgroundJobLogs;
using Singer.Core;
using Singer.Shared.Domain;

namespace Singer.BackgroundJob.RabbitMQ.BackgroundJobLogs;

/// <summary>
/// 后台任务日志详情表
/// </summary>
public class BackgroundJobLogDetail : IEntity<long>
{
    /// <summary>
    /// id
    /// </summary>
    [Column(IsPrimary = true, IsIdentity = true)]
    public long Id { get; protected set; }

    /// <summary>
    /// 日志表id
    /// </summary>
    public string LogId { get; init; }

    /// <summary>
    /// 日志类型
    /// </summary>
    [Column(MapType = typeof(string), DbType = "varchar(10)")]
    public BackgroundJobLogType LogType { get; init; }

    /// <summary>
    /// 是否是系统日志
    /// </summary>
    public bool IsSystem { get; init; }

    /// <summary>
    /// 日志内容
    /// </summary>
    [Column(DbType = "nvarchar(max)")]
    public string Content { get; init; }

    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime CreateTime { get; init; } = Cores.BeiJingNow;

    /// <summary>
    /// 默认构造函数（ORM使用）
    /// </summary>
    protected BackgroundJobLogDetail()
    {

    }

    /// <summary>
    /// 后台任务日志详情表 构造函数
    /// </summary>
    internal BackgroundJobLogDetail(BackgroundJobLogDetailDto dto)
    {
        LogId = dto.LogId;
        LogType = dto.LogType;
        IsSystem = dto.IsSystem;
        Content = dto.Content;
        CreateTime = dto.CreateTime;
    }
}
