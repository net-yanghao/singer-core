﻿using Singer.BackgroundJob.RabbitMQ.Contracts.BackgroundJobLogs;

namespace Singer.BackgroundJob.RabbitMQ.BackgroundJobLogs.Dtos;

/// <summary>
/// 后台任务日志详情数据
/// </summary>
public class BackgroundJobLogDetailDto
{
    /// <summary>
    /// 后台任务日志id
    /// </summary>
    public string LogId { get; set; }
    /// <summary>
    /// 日志类型
    /// </summary>
    public BackgroundJobLogType LogType { get; set; }
    /// <summary>
    /// 是否是系统日志
    /// </summary>
    public bool IsSystem { get; set; }
    /// <summary>
    /// 日志内容
    /// </summary>
    public string Content { get; set; }
    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime CreateTime { get; set; }
}
