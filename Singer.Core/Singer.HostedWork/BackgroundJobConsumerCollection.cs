﻿using System.Collections.ObjectModel;

namespace Singer.BackgroundJob.RabbitMQ;

/// <summary>
/// 后台任务 消费者上下文 列表
/// </summary>
public class BackgroundJobConsumerCollection : ReadOnlyCollection<ConsumerContext>, IDisposable
{
    public BackgroundJobConsumerCollection(IList<ConsumerContext> list) : base(list)
    {
    }

    public void Dispose()
    {
        foreach (var item in this)
        {
            item.Dispose();
        }
    }
}
