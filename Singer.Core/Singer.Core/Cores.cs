﻿namespace Singer.Core;

/// <summary>
/// 全局公共属性、方法
/// </summary>
public static class Cores
{
    /// <summary>
    /// 获取北京时间（不受服务器影响）
    /// </summary>
    public static DateTime BeiJingNow => DateTime.UtcNow.AddHours(8);
    /// <summary>
    /// 获取当前unix时间戳
    /// </summary>
    public static long TimeStamp => DateTimeOffset.Now.ToUnixTimeSeconds();
    /// <summary>
    /// 获取32位小写不带-的guid
    /// </summary>
    public static string Guid => System.Guid.NewGuid().ToString("N");
}