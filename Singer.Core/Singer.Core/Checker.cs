﻿using System.Text.RegularExpressions;

namespace Singer.Core;

/// <summary>
/// 数据检查 - 系统
/// </summary>
public static class Checker
{
    /// <summary>
    /// 判断字符串 不为null 且不为空字符串 且不是纯空格
    /// </summary>
    public static bool NotNullOrWhiteSpace(this string text) => !string.IsNullOrWhiteSpace(text);

    /// <summary>
    /// 检查字符串 不为null 且不为空字符串 且不是纯空格
    /// </summary>
    public static string CheckNotNullOrWhiteSpace(this string? text, string paramName = "", string msg = "")
    {
        if (!string.IsNullOrWhiteSpace(text))
            return text;
        if (!string.IsNullOrWhiteSpace(msg))
            throw new Exception(msg);
        if (!string.IsNullOrWhiteSpace(paramName))
            throw new ArgumentException($"The parameter '{paramName}' value cannot be null.");
        throw new ArgumentNullException();
    }

    /// <summary>
    /// 检查object不为null
    /// </summary>
    public static object CheckNotNull(this object? obj, string paramName = "", string msg = "")
    {
        if (obj != null)
            return obj;
        if (!string.IsNullOrWhiteSpace(msg))
            throw new Exception(msg);
        if (!string.IsNullOrWhiteSpace(paramName))
            throw new ArgumentException($"The parameter '{paramName}' value cannot be null.");
        throw new ArgumentNullException();
    }

    /// <summary>
    /// 检查枚举值是否合法
    /// </summary>
    public static TEnum CheckEnum<TEnum>(this TEnum? _enum, string msg = "") where TEnum : struct, Enum
    {
        if (_enum != null && Enum.IsDefined(_enum.Value))
            return _enum.Value;
        if (!string.IsNullOrWhiteSpace(msg))
            throw new Exception(msg);
        throw new ArgumentException($"The parameter '{typeof(TEnum).Name}' enum value error.");
    }

    /// <summary>
    /// 检查数字不可为0
    /// </summary>
    public static int CheckNotZero(this int? number, string paramName = "", string msg = "")
    {
        if (number != null && number != 0)
            return number.Value;
        if (!string.IsNullOrWhiteSpace(msg))
            throw new Exception(msg);
        if (!string.IsNullOrWhiteSpace(paramName))
            throw new ArgumentException($"The parameter '{paramName}' value can not be 0 or null.");
        throw new ArgumentNullException();
    }

    /// <summary>
    /// 检查数字不可为0
    /// </summary>
    public static long CheckNotZero(this long? number, string paramName = "", string msg = "")
    {
        if (number != null && number != 0)
            return number.Value;
        if (!string.IsNullOrWhiteSpace(msg))
            throw new Exception(msg);
        if (!string.IsNullOrWhiteSpace(paramName))
            throw new ArgumentException($"The parameter '{paramName}' value can not be 0 or null. ");
        throw new ArgumentNullException();
    }

    /// <summary>
    /// 检查数字必须大于0
    /// </summary>
    public static int CheckGreaterThanZero(this int? number, string paramName = "", string msg = "")
    {
        if (number > 0)
            return number.Value;
        if (!string.IsNullOrWhiteSpace(msg))
            throw new Exception(msg);
        if (!string.IsNullOrWhiteSpace(paramName))
            throw new ArgumentException($"The parameter '{paramName}' value must > 0 .");
        throw new ArgumentNullException();
    }

    /// <summary>
    /// 检查数字必须大于0
    /// </summary>
    public static long CheckGreaterThanZero(this long? number, string paramName = "", string msg = "")
    {
        if (number > 0)
            return number.Value;
        if (!string.IsNullOrWhiteSpace(msg))
            throw new Exception(msg);
        if (!string.IsNullOrWhiteSpace(paramName))
            throw new ArgumentException($"The parameter '{paramName}' value must > 0 . ");
        throw new ArgumentNullException();
    }

    /// <summary>
    /// 正则验证字符串是否是纯数字
    /// </summary>
    public static bool IsInteger(this string? txt)
    {
        if (string.IsNullOrWhiteSpace(txt))
            return false;
        if (!Regex.IsMatch(txt, @"^[0-9]*$"))
            return false;
        return true;
    }
}
