﻿using Newtonsoft.Json;

namespace Singer.Core.DataExtensions;

public static class ObjectExtensions
{
    /// <summary>
    /// 深克隆一个引用类型对象
    /// </summary>
    /// <typeparam name="T">对象类型</typeparam>
    /// <param name="obj">对象实例</param>
    /// <returns>克隆后的新对象</returns>
    public static T? DeepClone<T>(this T? obj)
    {
        if (obj == null)
            return default(T?);
        return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(obj));
    }
}
