﻿namespace Singer.Core.DataExtensions;

public static class StringExtensions
{
    /// <summary>
    /// 首字母小写
    /// </summary>
    public static string ToInitialLower(this string txt) => txt.Substring(0, 1).ToLower() + txt.Substring(1);
}
