﻿namespace Singer.Core.DataExtensions;

public static class DateTimeExtensions
{
    public static string ToTimeStr(this DateTime dateTime) => dateTime.ToString("yyyy-MM-dd HH:mm:ss");
    public static string ToDateStr(this DateTime dateTime) => dateTime.ToString("yyyy-MM-dd");
}
