﻿using System.Linq.Expressions;

namespace Singer.Core.DataExtensions;

public static class LinqExtensions
{
    /// <summary>
    /// 当条件成立时，给查询表达式添加where条件
    /// </summary>
    /// <param name="condition">判断条件</param>
    /// <param name="expWhere">判断条件为true时，添加的where条件</param>
    public static IQueryable<T> WhereIf<T>(this IQueryable<T> query, bool condition, Expression<Func<T, bool>> expWhere)
    {
        if(!condition)
            return query;
        return query.Where(expWhere);
    }

    /// <summary>
    /// 当前置条件成立时，给集合添加过滤条件
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list">集合</param>
    /// <param name="condition">前置条件</param>
    /// <param name="condition">前置条件为true时，添加的过滤条件</param>
    /// <returns>过滤后的集合</returns>
    public static IEnumerable<T> WhereIf<T>(this IEnumerable<T> list, bool condition, Func<T, bool> predicate)
    {
        if (list == null)
            return null;
        if (list.Count() == 0)
            return list;
        if (!condition)
            return list;
        return list.Where(predicate);
    }
}
