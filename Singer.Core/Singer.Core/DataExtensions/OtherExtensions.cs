﻿using System.Reflection;

namespace Singer.Core.DataExtensions;

public static class OtherExtensions
{
    /// <summary>
    /// 合并实体
    /// 将源实体的同名字段 赋值给目标实体
    /// </summary>
    /// <typeparam name="TTarget">合并后的实体 类型</typeparam>
    /// <typeparam name="TSource">包含数据的源实体 类型</typeparam>
    /// <param name="target">合并后的实体</param>
    /// <param name="source">包含数据的源实体</param>
    /// <param name="ignoreProperties">忽略的字段名</param>
    /// <returns>合并后的实体</returns>
    public static TTarget MergeEntity<TTarget, TSource>(this TTarget target, TSource source, List<string>? ignoreProperties)
    {
        // 获取源实体和目标实体的所有属性
        PropertyInfo[] targetProperties = typeof(TTarget).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        PropertyInfo[] sourceProperties = typeof(TSource).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (var sourceProperty in sourceProperties)
        {
            if (ignoreProperties?.Contains(sourceProperty.Name) == true)
            {
                continue; // 排除的字段
            }
            // 查找目标实体中是否存在同名属性
            var targetProperty = targetProperties.FirstOrDefault(prop => prop.Name == sourceProperty.Name);
            // 如果目标实体中存在同名属性且类型匹配
            if (targetProperty != null && targetProperty.CanWrite && targetProperty.PropertyType == sourceProperty.PropertyType)
            {
                // 获取源属性的值
                var value = sourceProperty.GetValue(source);
                // 将源属性值赋给目标属性
                targetProperty.SetValue(target, value);
            }
        }
        return target;
    }
}
