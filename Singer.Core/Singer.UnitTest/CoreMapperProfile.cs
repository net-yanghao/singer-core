﻿using AutoMapper;
using Singer.UnitTest.Models;

namespace Singer.UnitTest
{
    public class CoreMapperProfile : Profile
    {
        public CoreMapperProfile()
        {
            CreateMap<User, UserDto>().ForMember(des => des.Status,
                opt => opt.MapFrom(x => x.Status == 0 ? "正常" : "不可用")).ReverseMap();
        }
    }
}
