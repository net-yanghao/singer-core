﻿// See https://aka.ms/new-console-template for more information
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Singer.UnitTest;
using Singer.UnitTest.Models;

Console.WriteLine("Hello, World!");

IServiceCollection services = new ServiceCollection();
services.AddAutoMapper(typeof(CoreMapperProfile));

IServiceProvider serviceProvider = services.BuildServiceProvider();


IMapper _mapper = serviceProvider.GetRequiredService<IMapper>();
User user1 = new User
{
    Id = 1,
    Name = "km",
    Email = "31231@qq.com",
    Mobile = "186666666661",
    Password = "666",
    CreateTime = DateTime.Now,
    Status = 0
};
User user2 = new User
{
    Id = 2,
    Name = "qwer",
    Email = "312312qwer@qq.com",
    Password = "666",
    Mobile = "186666666662",
    CreateTime = DateTime.Now,
    Status = 1
};
List<User> users = new List<User>() { user1, user2 };
var userDtos = _mapper.Map<List<UserDto>>(users);





Console.ReadKey();
