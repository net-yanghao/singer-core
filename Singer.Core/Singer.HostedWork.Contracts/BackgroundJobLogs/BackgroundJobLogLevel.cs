﻿using System.ComponentModel;

namespace Singer.BackgroundJob.RabbitMQ.Contracts.BackgroundJobLogs;

/// <summary>
/// 后台任务日志等级
/// </summary>
public enum BackgroundJobLogLevel
{
    /// <summary>
    /// 忽略
    /// </summary>
    [Description("忽略")]
    Ignore = 0,
    /// <summary>
    /// 低
    /// </summary>
    [Description("低")]
    Low = 1,
    /// <summary>
    /// 中
    /// </summary>
    [Description("中")]
    Middle = 2,
    /// <summary>
    /// 高
    /// </summary>
    [Description("高")] 
    High = 3
}
