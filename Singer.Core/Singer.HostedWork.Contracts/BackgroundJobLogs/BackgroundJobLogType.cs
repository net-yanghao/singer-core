﻿using System.ComponentModel;

namespace Singer.BackgroundJob.RabbitMQ.Contracts.BackgroundJobLogs;

/// <summary>
/// 后台任务日志类型
/// </summary>
public enum BackgroundJobLogType
{
    /// <summary>
    /// 信息
    /// </summary>
    [Description("信息")] 
    Info = 0,
    /// <summary>
    /// 警告
    /// </summary>
    [Description("警告")]
    Warning = 1,
    /// <summary>
    /// 调试
    /// </summary>
    [Description("调试")]
    Debug = 2,
    /// <summary>
    /// 异常
    /// </summary>
    [Description("异常")]
    Error = 3,
}
