﻿using System.ComponentModel;
namespace Singer.BackgroundJob.RabbitMQ.Contracts.BackgroundJobLogs;

/// <summary>
/// 后台任务状态
/// </summary>
public enum BackgroundJobLogStatus
{
    /// <summary>
    /// 队列中
    /// </summary>
    [Description("队列中")]
    InQueue = 0,
    
    /// <summary>
    /// 初始化
    /// </summary>
    [Description("初始化")]
    Init = 1,
    
    /// <summary>
    /// 运行中
    /// </summary>
    [Description("运行中")]
    Running = 2,

    /// <summary>
    /// 取消中
    /// </summary>
    [Description("取消中")]
    Cancelling = 3,

    /// <summary>
    /// 已取消
    /// </summary>
    [Description("已取消")]
    Canceled = 4,

    /// <summary>
    /// 重启中
    /// </summary>
    [Description("重启中")]
    Restarting = 5,

    /// <summary>
    /// 失败
    /// </summary>
    [Description("失败")]
    Failed = 6,

    /// <summary>
    /// 成功
    /// </summary>
    [Description("成功")]
    Success = 10,
}
