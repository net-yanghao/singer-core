﻿using System.Diagnostics.CodeAnalysis;

namespace Singer.BackgroundJob.RabbitMQ.Contracts;

/// <summary>
/// 后台任务触发器 接口
/// </summary>
public interface IBackgroundJobTrigger
{
    /// <summary>
    /// 发布消息，触发对应的后台任务
    /// </summary>
    /// <typeparam name="TArgs">后台任务消息类型</typeparam>
    /// <param name="args">消息</param>
    Task<string> PublishAsync<TArgs>([NotNull] TArgs args) where TArgs : IBackgroundJobArgs;

    /// <summary>
    /// 手动取消任务
    /// </summary>
    /// <param name="jobLogId">后台任务日志id</param>
    Task CancellingAsync(string jobLogId);

    /// <summary>
    /// 手动重启任务
    /// </summary>
    /// <param name="jobLogId">后台任务日志id</param>
    Task RestartingAsync(string jobLogId);
}
