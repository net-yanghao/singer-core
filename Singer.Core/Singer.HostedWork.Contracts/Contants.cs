﻿
namespace Singer.BackgroundJob.RabbitMQ.Contracts;

/// <summary>
/// 后台任务 常量
/// </summary>
public class Contants
{
    /// <summary>
    /// 默认消费者数量
    /// </summary>
    // public const int DEFAULT_CONSUMER_COUNT = 3;

    /// <summary>
    /// 默认最大重试次数
    /// </summary>
    public const int DEFAULT_MAX_RETRY_COUNT = 2;

    /// <summary>
    /// 默认 交换机名
    /// </summary>
    public const string DEFAULT_EXCHANGENAME = "exchange_singer_hostedwork";

    /// <summary>
    /// 默认 任务取消队列名
    /// </summary>
    public const string DEFAULT_QUEUENAME_CANCEL_TASK = "queue_singer_hostedwork_canceltask";

    /// <summary>
    /// 默认 任务取消队列路由键
    /// </summary>
    public const string DEFAULT_ROUTINGKEY_CANCEL_TASK = "routingkey_singer_houstedwork_canceltask";

    /// <summary>
    /// 消费者标识，前缀
    /// </summary>
    public const string CONSUMERNAME_PREFIX = "consumer_singer_hostedwork_";
    /// <summary>
    /// 队列名，前缀
    /// </summary>
    public const string QUEUENAME_PREFIX = "queue_singer_hostedwork_";
    /// <summary>
    /// 路由键，前缀
    /// </summary>
    public const string ROUTINGKEY_PREFIX = "routingkey_singer_hostedwork_";

}
