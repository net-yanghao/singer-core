﻿namespace Singer.BackgroundJob.RabbitMQ.Contracts;

/// <summary>
/// 后台任务 消息队列 推送消息模型
/// </summary>
public class BackgroundJobMessage
{
    /// <summary>
    /// 后台任务日志id
    /// </summary>
    public string JobLogId { get; set; }
    /// <summary>
    /// 当前租户id 多租户时需要
    /// </summary>
    public string? CurrentTenantId { get; set; }
    /// <summary>
    /// 触发任务的当前用户id
    /// </summary>
    public string? CurrentUserId { get; set; }

    public BackgroundJobMessage(string jobLogId, string? currentTenantId, string? currentUserId)
    {
        JobLogId = jobLogId;
        CurrentTenantId = currentTenantId;
        CurrentUserId = currentUserId;
    }
}
