﻿
using Singer.BackgroundJob.RabbitMQ.Contracts.BackgroundJobLogs;

namespace Singer.BackgroundJob.RabbitMQ.Contracts;

/// <summary>
/// 后台任务执行器 特性
/// </summary>
[AttributeUsage(AttributeTargets.Class)]
public class BackgroundJobExecutorAttribute : Attribute
{
    /// <summary>
    /// 任务参数 类型
    /// </summary>
    public Type ArgsType { get; init; }

    /// <summary>
    /// 任务执行器标题
    /// </summary>
    public string Title { get; init; }

    /// <summary>
    /// 是否是系统级别任务
    /// </summary>
    public bool IsSystemJob { get; init; }

    /// <summary>
    /// 日志等级
    /// </summary>
    public BackgroundJobLogLevel LogLevel { get; init; } = BackgroundJobLogLevel.Middle;

    /// <summary>
    /// 是否失败后重试
    /// </summary>
    public bool FailRetry { get; init; }

    /// <summary>
    /// 最大重试次数
    /// </summary>
    public int MaxRetryCount { get; init; } = Contants.DEFAULT_MAX_RETRY_COUNT;

    public BackgroundJobExecutorAttribute(Type argsType, string title)
    {
        ArgsType = argsType;
        Title = title;
    }
}
