﻿using static Singer.BackgroundJob.RabbitMQ.Contracts.Contants;

namespace Singer.BackgroundJob.RabbitMQ.Contracts;

/// <summary>
/// 后台任务 配置参数
/// </summary>
public class BackgroundJobOptions
{
    public const string ConfigKey = "BackgroundJob.RabbitMQ";
    /// <summary>
    /// RabbitMQ连接名，不传时使用默认连接
    /// </summary>
    public string? ConnectionName { get; set; }
    /// <summary>
    /// 后台任务交换机名
    /// </summary>
    public string ExchangeName { get; set; } = DEFAULT_EXCHANGENAME;
    /// <summary>
    /// 消费者数量
    /// </summary>
    // public ushort ConsumerCount { get; set; } = DEFAULT_CONSUMER_COUNT;
    /// <summary>
    /// 每个消费者预取消息数量（消费者同时消费的消息数量）
    /// </summary>
    public ushort ConsumerQos { get; set; } = 3;
    /// <summary>
    /// 失败时，等待重试的时间间隔（秒）
    /// </summary>
    public int FailedRetryDelaySeconds { get; set; } = 10;
    /// <summary>
    /// 失败重试
    /// </summary>
    // public bool FailedRetry { get; set; } = false;
    /// <summary>
    /// 最大重试次数
    /// </summary>
    // public ushort MaxRetryCount { get; set; } = 2;
}
