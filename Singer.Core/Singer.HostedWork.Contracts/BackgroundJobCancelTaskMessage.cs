﻿namespace Singer.BackgroundJob.RabbitMQ.Contracts;

/// <summary>
/// 后台任务取消队列 消息模型
/// </summary>
public class BackgroundJobCancelTaskMessage
{
    /// <summary>
    /// 后台任务日志id
    /// </summary>
    public string JobLogId { get; set; }

    public BackgroundJobCancelTaskMessage(string jobLogId)
    {
        JobLogId = jobLogId;
    }
}
