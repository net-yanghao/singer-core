﻿
namespace Singer.BackgroundJob.RabbitMQ.Contracts;

/// <summary>
/// 后台任务执行器 接口
/// </summary>
public interface IBackgroundJobExecutor
{
    /// <summary>
    /// 任务执行逻辑
    /// </summary>
    Task ExecuteAsync();
}

/// <summary>
/// 后台任务执行器 泛型接口
/// </summary>
/// <typeparam name="TArgs">对应的任务参数模型</typeparam>
public interface IBackgroundJobExecutor<TArgs> : IBackgroundJobExecutor 
    where TArgs : IBackgroundJobArgs
{
}
