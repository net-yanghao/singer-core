﻿using FreeSql;

namespace Singer.Middleware.FreeSql;

/// <summary>
/// FreeSql 配置模型
/// </summary>
public class FreeSqlOptions
{
    public const string ConfigKey = "FreeSql";
    /// <summary>
    /// 数据库类型
    /// </summary>
    public DataType DataType { get; set; } = DataType.SqlServer;
    /// <summary>
    /// 是否自动同步数据库表结构。开发环境很快乐，生产环境千万别用！
    /// </summary>
    public bool AutoSyncStructure { get; set; } = false;
    /// <summary>
    /// 是否开启Aop审计功能，操作实体时自动加上创建人修改人信息
    /// </summary>
    public bool UseAopAudit { get; set; } = false;
    /// <summary>
    /// 链接字符串
    /// </summary>
    public string ConnectionString { get; set; }

    public void Validate()
    {
        if (string.IsNullOrWhiteSpace(ConnectionString))
            throw new Exception($"【FreeSql】The config item '{nameof(ConnectionString)}' value can not be null.");
        if (!Enum.IsDefined(typeof(DataType), DataType))
            throw new Exception($"【FreeSql】The config item '{nameof(DataType)}' value error.");
    }
}
