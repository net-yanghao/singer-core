﻿using System.Linq.Expressions;

namespace Singer.Middleware.FreeSql;

/// <summary>
/// FreeSql仓储基类
/// </summary>
/// <typeparam name="TEntity">实体模型</typeparam>
public interface IBaseRepository<TEntity> : global::FreeSql.IBaseRepository<TEntity>
    where TEntity : class, IEntity
{
    /// <summary>
    /// 判断数据是否存在
    /// </summary>
    bool Any(Expression<Func<TEntity, bool>>? exp = null);
    /// <summary>
    /// 判断数据是否存在
    /// </summary>
    Task<bool> AnyAsync(Expression<Func<TEntity, bool>>? exp = null, CancellationToken cancellationToken = default);
    /// <summary>
    /// 获取单条数据，返回实体
    /// </summary>
    TEntity? Get(Expression<Func<TEntity, bool>>? exp = null);
    /// <summary>
    /// 获取单条数据，返回实体
    /// </summary>
    Task<TEntity?> GetAsync(Expression<Func<TEntity, bool>>? exp = null, CancellationToken cancellationToken = default);
    /// <summary>
    /// 获取单条数据，返回Dto
    /// </summary>
    TDto? Get<TDto>(Expression<Func<TEntity, bool>>? exp = null) where TDto : class;
    /// <summary>
    /// 获取单条数据，返回Dto
    /// </summary>
    Task<TDto?> GetAsync<TDto>(Expression<Func<TEntity, bool>>? exp = null, CancellationToken cancellationToken = default) where TDto : class;
}
