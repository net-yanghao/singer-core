﻿namespace Singer.Middleware.EFCore;

/// <summary>
/// EFCore 配置参数模型
/// </summary>
public class EFCoreOptions
{
    public const string ConfigKey = "EFCore";
    /// <summary>
    /// 数据库类型
    /// </summary>
    public DatabaseType DataType { get; set; } = DatabaseType.SqlServer;
    /// <summary>
    /// 连接字符串
    /// </summary>
    public string ConnectionString { get; set; }
    /// <summary>
    /// 是否开启Aop审计功能，操作实体时自动加上创建人修改人信息
    /// </summary>
    public bool UseAopAudit { get; set; } = false;


    public void Validate()
    {
        if (string.IsNullOrWhiteSpace(ConnectionString))

            throw new Exception($"【EFCore】The config item '{nameof(ConnectionString)}' value can not be null.");
        if (!Enum.IsDefined(typeof(DatabaseType), DataType))
            throw new Exception($"【EFCore】The config item '{nameof(DataType)}' value error.");
    }
}
