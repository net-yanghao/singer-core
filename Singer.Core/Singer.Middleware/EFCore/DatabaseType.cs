﻿namespace Singer.Middleware.EFCore;

/// <summary>
/// 数据库类型
/// </summary>
public enum DatabaseType
{
    MySql, 
    SqlServer, 
    PostgreSQL, 
    Oracle, 
    Sqlite
}
