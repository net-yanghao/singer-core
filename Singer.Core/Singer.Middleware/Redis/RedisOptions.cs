﻿namespace Singer.Middleware.Redis;

/// <summary>
/// Redis服务配置
/// </summary>
public class RedisOptions
{
    public const string ConfigKey = "Redis";
    /// <summary>
    /// 连接名-连接字符串 字典 key: connectionName  value: connectionString
    /// </summary>
    public Dictionary<string, string> Connections { get; set; } = new Dictionary<string, string>();
    /// <summary>
    /// 项目实例名 作为RedisKey的前缀，非必填。
    /// </summary>
    public string InstanceName { get; set; } = "";

    public const string DefaultName = "Default";

    public string Get(string connectionName)
    {
        if (Connections.TryGetValue(connectionName, out var value))
        {
            if (string.IsNullOrWhiteSpace(value))
                throw new Exception($"【Redis】 The Connection '{connectionName}' value can not be null.");
            return value;
        }
        throw new Exception($"【Redis】 The Connection '{connectionName}' was not found.");
    }

    public void Validate()
    {
        if (Connections.Count == 0)
            throw new Exception($"【Redis】The config item '{nameof(Connections)}' value can not be null.");
        if (!Connections.ContainsKey(DefaultName) || string.IsNullOrWhiteSpace(Connections[DefaultName]))
            throw new Exception($"【Redis】The config item '{nameof(Connections)}' must has a Default Connection $'{DefaultName}'");
    }
}
