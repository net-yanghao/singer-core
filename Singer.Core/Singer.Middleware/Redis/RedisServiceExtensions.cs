﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Singer.Middleware.Redis;

/// <summary>
/// Redis服务拓展
/// </summary>
public static class RedisServiceExtensions
{
    public static void AddRedis(this IServiceCollection services, IConfiguration configuration)
    {
        var config = configuration.GetSection(RedisOptions.ConfigKey);
        RedisOptions? options = config.Get<RedisOptions>();
        if (options == null)
            throw new Exception("【Redis】Config was not found.");
        options.Validate();
        services.Configure<RedisOptions>(config);
        services.TryAddSingleton<IRedisConnectionPool, RedisConnectionPool>();
    }

    public static void AddRedis(this IServiceCollection services, Action<RedisOptions> options)
    {
        services.Configure<RedisOptions>(options);
        services.TryAddSingleton<IRedisConnectionPool, RedisConnectionPool>();
    }
}
