﻿using StackExchange.Redis;

namespace Singer.Middleware.Redis;

/// <summary>
/// Redis连接池服务 接口
/// </summary>
public interface IRedisConnectionPool : IDisposable
{
    /// <summary>
    /// 获取指定连接
    /// </summary>
    /// <param name="connectionName">redis连接名</param>
    IConnectionMultiplexer GetConnection(string connectionName);
    /// <summary>
    /// 获取指定连接的IDatabase
    /// </summary>
    /// <param name="db">dbId</param>
    /// <param name="connectionName">redis连接名，默认为Default</param>
    IDatabase GetDatabase(int db = -1, string? connectionName = null);
}
