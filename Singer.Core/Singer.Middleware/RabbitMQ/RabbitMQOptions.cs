﻿namespace Singer.Middleware.RabbitMQ;

/// <summary>
/// RabbitMQ服务配置
/// </summary>
public class RabbitMQOptions
{
    public const string ConfigKey = "RabbitMQ";

    /// <summary>
    /// 连接配置
    /// </summary>
    public RabbitMQConnections Connections { get; set; }
}
