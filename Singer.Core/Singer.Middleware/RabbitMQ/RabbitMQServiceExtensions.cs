﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Singer.Middleware.RabbitMQ;

/// <summary>
/// RabbitMQ服务拓展
/// </summary>
public static class RabbitMQServiceExtensions
{
    public static void AddRabbitMQ(this IServiceCollection services, IConfiguration configuration)
    {
        var config = configuration.GetSection(RabbitMQOptions.ConfigKey);
        RabbitMQOptions? options = config.Get<RabbitMQOptions>();
        if (options == null)
            throw new Exception("【RabbitMQ】Config was not found.");
        services.Configure<RabbitMQOptions>(config);
        services.TryAddSingleton<IRabbitMQConnectionPool, RabbitMQConnectionPool>();
    }
}
