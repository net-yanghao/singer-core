﻿using RabbitMQ.Client;
namespace Singer.Middleware.RabbitMQ;

/// <summary>
/// RabbitMQ 连接池 接口
/// </summary>
public interface IRabbitMQConnectionPool : IDisposable
{
    /// <summary>
    /// 创建/获取 RabbitMQ连接
    /// </summary>
    /// <param name="connectionName">连接名</param>
    IConnection Get(string? connectionName = null);
}


