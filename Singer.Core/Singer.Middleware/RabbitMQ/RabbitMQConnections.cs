﻿using RabbitMQ.Client;

namespace Singer.Middleware.RabbitMQ;

/// <summary>
/// RabbitMQ连接配置
/// </summary>
public class RabbitMQConnections : Dictionary<string, ConnectionFactory>
{
    /// <summary>
    /// 默认连接名
    /// </summary>
    public const string DefaultConnectionName = "Default";

    /// <summary>
    /// 获取连接
    /// </summary>
    /// <param name="connectionName">连接名</param>
    public ConnectionFactory Get(string connectionName = DefaultConnectionName)
    {
        if (TryGetValue(connectionName, out var connectionFactory))
        {
            return connectionFactory;
        }
        throw new Exception($"【RabbitMQ】The connection '{connectionName}' config was not found.");
    }
}
