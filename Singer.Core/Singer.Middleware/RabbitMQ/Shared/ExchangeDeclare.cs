﻿using RabbitMQ.Client;
using System.Diagnostics.CodeAnalysis;

namespace Singer.Middleware.RabbitMQ.Shared;

/// <summary>
/// RabbitMQ 交换机声明
/// </summary>
public struct ExchangeDeclare
{
    /// <summary>
    /// 交换机名
    /// </summary>
    public string ExchangeName { get; }
    /// <summary>
    /// 交换机类型
    /// </summary>
    public ExchangeTypeEnum Type { get; }
    /// <summary>
    /// 是否持久化
    /// </summary>
    public bool Durable { get; set; }
    /// <summary>
    /// 是否自动删除
    /// </summary>
    public bool AutoDelete { get; set; }
    /// <summary>
    /// 其他参数
    /// </summary>
    public IDictionary<string, object>? Arguments { get; }

    /// <summary>
    /// RabbitMQ 交换机定义
    /// </summary>
    /// <param name="exchangeName">交换机名</param>
    /// <param name="type">交换机类型</param>
    /// <param name="durable">是否持久化</param>
    /// <param name="autoDelete">是否自动删除</param>
    public ExchangeDeclare(
        [NotNull] string exchangeName,
        ExchangeTypeEnum type = ExchangeTypeEnum.Direct,
        bool durable = false,
        bool autoDelete = false,
        IDictionary<string, object>? arguments = null)
    {
        ExchangeName = exchangeName;
        Type = type;
        Durable = durable;
        AutoDelete = autoDelete;
        Arguments = arguments;
    }

    /// <summary>
    /// 声明交换机
    /// </summary>
    /// <param name="channel">RabbitMQ信道</param>
    public void Declare(IModel channel)
    {
        channel.ExchangeDeclare(
            exchange: ExchangeName,
            type: Type.ToString().ToLower(),
            durable: Durable,
            autoDelete: AutoDelete,
            arguments: Arguments
            );
    }
}
