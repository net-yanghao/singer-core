﻿namespace Singer.Middleware.RabbitMQ.Shared;

/// <summary>
/// RabbitMQ交换机类型
/// </summary>
public enum ExchangeTypeEnum
{
    /// <summary>
    /// 直接交换机
    /// </summary>
    Direct,
    /// <summary>
    /// 扇形交换机
    /// </summary>
    Fanout,
    /// <summary>
    /// 主题交换机
    /// </summary>
    Topic,
    /// <summary>
    /// 头交换机
    /// </summary>
    Headers
}
