﻿using RabbitMQ.Client;
using System.Diagnostics.CodeAnalysis;

namespace Singer.Middleware.RabbitMQ.Shared;

/// <summary>
/// RabbitMQ 队列声明
/// </summary>
public struct QueueDeclare
{
    /// <summary>
    /// 队列名
    /// 队列的唯一标识
    /// </summary>
    public string QueueName { get; set; }
    /// <summary>
    /// 是否持久化
    /// true：持久，备份到磁盘，mq服务重启之后还在；
    /// false：非持久，在内存中，mq服务重启之后就没了。
    /// </summary>
    public bool Durable { get; set; }
    /// <summary>
    /// 是否排他
    /// true：仅对首次声明队列的连接使用，连接关闭后就删除。
    /// </summary>
    public bool Exclusive { get; set; }
    /// <summary>
    /// 是否自动删除
    /// ture：当没有生产者和消费者都断开连接时，自动删除队列。
    /// </summary>
    public bool AutoDelete { get; set; }
    /// <summary>
    /// 其他拓展参数
    /// </summary>
    public IDictionary<string, object>? Arguments { get; }

    /// <summary>
    /// RabbitMQ 队列声明
    /// </summary>
    /// <param name="queueName">队列名</param>
    /// <param name="durable">是否持久化</param>
    /// <param name="exclusive">是否排他</param>
    /// <param name="autoDelete">是否自动删除</param>
    /// <param name="arguments">其他参数</param>
    public QueueDeclare(
        [NotNull] string queueName,
        bool durable = true,
        bool exclusive = false,
        bool autoDelete = false,
        IDictionary<string, object>? arguments = null)
    {
        QueueName = queueName;
        Durable = durable;
        Exclusive = exclusive;
        AutoDelete = autoDelete;
        Arguments = arguments;
    }

    /// <summary>
    /// 声明队列
    /// </summary>
    /// <param name="channel">RabbitMQ信道</param>
    public QueueDeclareOk Declare(IModel channel)
    {
        return channel.QueueDeclare(
            queue: QueueName,
            durable: Durable,
            exclusive: Exclusive,
            autoDelete: AutoDelete,
            arguments: Arguments
        );
    }
}
