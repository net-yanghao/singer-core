﻿using System.Security.Claims;

namespace Singer.Middleware.JwtAuth;

/// <summary>
/// JwtToken管理
/// </summary>
public interface IJwtAuthTokenManager
{
    /// <summary>
    /// Jwt服务配置
    /// </summary>
    JwtOptions JwtAuthOptions { get; }

    /// <summary>
    /// 根据有效身份载荷生成JwtToken并添加到缓存中
    /// </summary>
    Task<string> CreateTokenAsync(IEnumerable<Claim> claims);

    /// <summary>
    /// 从缓存中删除token
    /// </summary>
    Task DeleteTokenAsync(string token);

    /// <summary>
    /// 验证token是否存在缓存中
    /// </summary>
    Task<bool> ExistsTokenAsync(string token);
}
