﻿using Singer.Core;

namespace Singer.Middleware.JwtAuth;

/// <summary>
/// Jwt服务配置
/// </summary>
public class JwtOptions
{
    public const string ConfigKey = "JwtAuth";
    public const string TokenCacheKey = "jwttokens";
    public const int RedisDB = 3;
    public static string GetTokenCacheKey(string token) => TokenCacheKey + ":" + Tools.MD5(token);
    /// <summary>
    /// 签发人
    /// </summary>
    public string Issuer { get; set; } = "";
    /// <summary>
    /// 用户
    /// </summary>
    public string Audience { get; set; } = "";
    /// <summary>
    /// 对称可逆加密 秘钥
    /// </summary>
    public string SecurityKey { get; set; }
    /// <summary>
    /// token有效时间，多少小时之后过期
    /// </summary>
    public double ExpirationHour { get; set; } = 24;
    public void Validate()
    {
        if (ExpirationHour <= 0)
            throw new Exception($"【JwtAuth】The config item '{nameof(ExpirationHour)}' value must > 0 .");
        if (string.IsNullOrWhiteSpace(SecurityKey))
            throw new Exception($"【JwtAuth】The config item '{nameof(SecurityKey)}' value can not be null.");
    }
}
