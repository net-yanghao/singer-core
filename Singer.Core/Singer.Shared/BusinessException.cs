﻿namespace Singer.Shared;

/// <summary>
/// 业务异常
/// </summary>
public class BusinessException : CoreException
{
    /// <summary>
    /// 业务异常
    /// </summary>
    /// <param name="message">异常提示信息</param>
    public BusinessException(string message) : base(message, ErrorCode.Business)
    {
    }
}
