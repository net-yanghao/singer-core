﻿namespace Singer.Shared.Domain;

/// <summary>
/// 创建人 接口
/// </summary>
public interface ICreator
{
    /// <summary>
    /// 创建人id
    /// </summary>
    string? CreatorId { get; }
    /// <summary>
    /// 创建人姓名
    /// </summary>
    string? CreatorName { get; }
    /// <summary>
    /// 创建时间
    /// </summary>
    DateTime CreateTime { get; }
}
