﻿namespace Singer.Shared.Domain;

/// <summary>
/// 软删除 接口
/// </summary>
public interface ISoftDelete
{
    /// <summary>
    /// 是否已删除
    /// </summary>
    bool IsDeleted { get; }
    /// <summary>
    /// 删除人id
    /// </summary>
    string? DeleterId { get; }
    /// <summary>
    /// 删除人姓名
    /// </summary>
    string? DeleterName { get; }
    /// <summary>
    /// 删除时间
    /// </summary>
    DateTime? DeleteTime { get; }
}
