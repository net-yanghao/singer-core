﻿namespace Singer.Shared.Domain;

/// <summary>
/// 修改人 接口
/// </summary>
public interface IChanger
{
    /// <summary>
    /// 修改人id
    /// </summary>
    string? ChangerId { get; }
    /// <summary>
    /// 修改人姓名
    /// </summary>
    string? ChangerName { get; }
    /// <summary>
    /// 修改时间
    /// </summary>
    DateTime? ChangeTime { get; }
}
