﻿namespace Singer.Shared.Domain;

/// <summary>
/// 实体基类 接口
/// </summary>
public interface IEntity
{

}

/// <summary>
/// 实体基类 接口
/// </summary>
/// <typeparam name="T">主键Id的类型</typeparam>
public interface IEntity<T> : IEntity
{
    T Id { get; }
}
