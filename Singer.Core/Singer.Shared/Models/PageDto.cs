﻿namespace Singer.Shared.Models;

/// <summary>
/// 分页查询输入模型
/// </summary>
public class PageDto
{
    /// <summary>
    /// 页码
    /// </summary>
    public int PageIndex { get; set; }

    /// <summary>
    /// 每页条数
    /// </summary>
    public int PageSize { get; set; }
}

/// <summary>
/// 分页查询输出模型
/// </summary>
/// <typeparam name="T">实体类型</typeparam>
public class PageDto<T> : PageDto
{
    /// <summary>
    /// 数据总数
    /// </summary>
    public int Count { get; set; }

    /// <summary>
    /// 总页数
    /// </summary>
    public int PageCount { get; set; }

    /// <summary>
    /// 数据列表
    /// </summary>
    public List<T> List { get; set; }

    /// <summary>
    /// 分页查询输出模型
    /// </summary>
    /// <param name="list">当前页的数据列表</param>
    /// <param name="page">分页查询入参</param>
    /// <param name="count">数据总数</param>
    public PageDto(List<T> list, PageDto page, int count)
    {
        PageIndex = page.PageIndex;
        PageSize = page.PageSize;
        List = list;
        PageCount = (list.Count + page.PageSize - 1) / page.PageSize;
        Count = count;
    }
}