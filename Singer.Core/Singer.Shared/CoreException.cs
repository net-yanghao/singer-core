﻿namespace Singer.Shared;

/// <summary>
/// 统一异常
/// </summary>
public class CoreException : Exception
{
    public string Code { get; set; }

    public CoreException(string message, ErrorCode errorCode = ErrorCode.Normal) : base(message)
    {
        Code = errorCode.ToString();
    }

    public CoreException(string message, string errorCode) : base(message)
    {
        Code = errorCode;
    }
}
