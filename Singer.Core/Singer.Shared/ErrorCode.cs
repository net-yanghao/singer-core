﻿namespace Singer.Shared;

/// <summary>
/// 异常代码
/// </summary>
public enum ErrorCode
{
    /// <summary>
    /// 普通异常
    /// </summary>
    Normal = 0,
    /// <summary>
    /// 业务异常
    /// </summary>
    Business = 1,
    /// <summary>
    /// 身份认证异常
    /// </summary>
    Auth = 2,
    /// <summary>
    /// 接口参数异常
    /// </summary>
    Parameter = 3,
}
