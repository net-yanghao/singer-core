﻿using Singer.Shared;
using System.Text.RegularExpressions;

namespace Singer.Shared;

/// <summary>
/// 数据检查 - 业务
/// </summary>
public static class BusinessChecker
{
    /// <summary>
    /// 判断字符串 是否 不为null 且不为空字符串 且不是纯空格
    /// </summary>
    public static bool NotNullOrWhiteSpace(this string? text) => !string.IsNullOrWhiteSpace(text);

    /// <summary>
    /// 检查字符串 不为null 且不为空字符串
    /// </summary>
    public static string CheckNotNullOrEmpty(this string? text, string msg)
    {
        if (string.IsNullOrEmpty(text))
            throw new BusinessException(msg);
        return text;
    }

    /// <summary>
    /// 检查字符串 不为null 且不为空字符串 且不是纯空格
    /// </summary>
    public static string CheckNotNullOrWhiteSpace(this string? text, string msg)
    {
        if (string.IsNullOrWhiteSpace(text))
            throw new BusinessException(msg);
        return text;
    }

    /// <summary>
    /// 检查object不为null
    /// </summary>
    public static object CheckNotNull(this object? obj, string msg)
    {
        if (obj == null)
            throw new Exception(msg);
        return obj;
    }

    /// <summary>
    /// 检查枚举值是否合法
    /// </summary>
    public static TEnum CheckEnum<TEnum>(this TEnum? _enum, string msg) where TEnum : struct, Enum
    {
        if (_enum == null || !Enum.IsDefined(_enum.Value))
            throw new BusinessException(msg);
        return _enum.Value;
    }

    /// <summary>
    /// 检查数字不可为0
    /// </summary>
    public static int CheckNotZero(this int? number, string msg)
    {
        if (number == null || number == 0)
            throw new BusinessException(msg);
        return number.Value;
    }

    /// <summary>
    /// 检查数字不可为0
    /// </summary>
    public static long CheckNotZero(this long? number, string msg)
    {
        if (number == null || number == 0)
            throw new BusinessException(msg);
        return number.Value;
    }

    /// <summary>
    /// 检查数字必须大于0
    /// </summary>
    public static int CheckGreaterThanZero(this int? number, string msg)
    {
        if (number == null || number <= 0)
            throw new BusinessException(msg);
        return number.Value;
    }

    /// <summary>
    /// 检查数字必须大于0
    /// </summary>
    public static long CheckGreaterThanZero(this long? number, string msg)
    {
        if (number == null || number <= 0)
            throw new BusinessException(msg);
        return number.Value;
    }

    /// <summary>
    /// 正则验证字符串是否是纯数字
    /// </summary>
    public static bool IsInteger(this string? txt)
    {
        if (string.IsNullOrWhiteSpace(txt))
            return false;
        if (!Regex.IsMatch(txt, @"^[0-9]*$"))
            return false;
        return true;
    }

    /// <summary>
    /// 正则验证字符串是否是手机号格式
    /// </summary>
    public static bool IsMobile(this string? mobile)
    {
        if (string.IsNullOrWhiteSpace(mobile))
            return false;
        if (!Regex.IsMatch(mobile, @"^[1]+[3,4,5,6,7,8,9]\d{9}$")) return false;
        return true;
    }

    /// <summary>
    /// 正则验证字符串是否是邮箱格式
    /// </summary>
    public static bool IsEmail(this string? email)
    {
        email = email ?? "";
        if (Regex.IsMatch(email, "\\s")) return false;
        var index1 = email.IndexOf('@');
        if (index1 <= 0) return false;
        var index2 = email.IndexOf('.', index1);
        if (index2 <= 0 || index2 < index1 + 2) return false;
        return true;
    }
}
