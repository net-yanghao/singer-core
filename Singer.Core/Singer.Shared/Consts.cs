﻿namespace Singer.Shared;

/// <summary>
/// 公共常量类
/// </summary>
public static class Consts
{
    /// <summary>
    /// 项目团队名称
    /// </summary>
    public const string PROJECT_TEAM = "Singer";
}
