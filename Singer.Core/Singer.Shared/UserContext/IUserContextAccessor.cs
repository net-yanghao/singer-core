﻿namespace Singer.Shared.UserContext;

/// <summary>
/// 用户信息访问器 接口
/// </summary>
public interface IUserContextAccessor
{
    /// <summary>
    /// 当前用户信息
    /// </summary>
    IUserContext? UserContext { get; set; }
}
