﻿namespace Singer.Shared.UserContext;

/// <summary>
/// 用户信息访问器
/// </summary>
public class UserContextAccessor : IUserContextAccessor
{
    AsyncLocal<IUserContext?> _asyncLocal { get; set; } = new AsyncLocal<IUserContext?>();

    public IUserContext? UserContext
    {
        get => _asyncLocal.Value;
        set => _asyncLocal.Value = value;
    }
}
