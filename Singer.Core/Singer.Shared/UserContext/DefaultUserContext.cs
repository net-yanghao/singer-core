﻿namespace Singer.Shared.UserContext;

/// <summary>
/// 默认当前用户信息上下文
/// </summary>
public class DefaultUserContext : IUserContext
{
    /// <summary>
    /// 当前登录ip
    /// </summary>
    public string? Ip { get; set; }
    /// <summary>
    /// token
    /// </summary>
    public string? Token { get; set; }
    /// <summary>
    /// 当前租户id
    /// </summary>
    public string? TenantId { get; set; }
    /// <summary>
    /// 当前租户唯一标识码
    /// </summary>
    public string? TenantCode { get; set; }
    /// <summary>
    /// 当前租户名
    /// </summary>
    public string? TenantName { get; set; }
    /// <summary>
    /// 当前用户id
    /// </summary>
    public string? UserId { get; set; }
    /// <summary>
    /// 用户姓名
    /// </summary>
    public string? UserName { get; set; }
    /// <summary>
    /// 手机号
    /// </summary>
    public string? Mobile { get; set; }
    /// <summary>
    /// 拓展信息
    /// </summary>
    public IDictionary<string, string>? Extras { get; set; } = new Dictionary<string, string>();
}
