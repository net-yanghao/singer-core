﻿namespace Singer.Shared.UserContext;

/// <summary>
/// 当前用户信息上下文
/// </summary>
public interface IUserContext
{
    /// <summary>
    /// 当前登录ip
    /// </summary>
    string? Ip { get; set; }
    /// <summary>
    /// token
    /// </summary>
    string? Token { get; set; }
    /// <summary>
    /// 当前租户id
    /// </summary>
    string? TenantId { get; set; }
    /// <summary>
    /// 当前租户唯一标识码
    /// </summary>
    string? TenantCode { get; set; }
    /// <summary>
    /// 当前租户名
    /// </summary>
    string? TenantName { get; set; }
    /// <summary>
    /// 当前用户id
    /// </summary>
    string? UserId { get; set; }
    /// <summary>
    /// 用户姓名
    /// </summary>
    string? UserName { get; set; }
    /// <summary>
    /// 手机号
    /// </summary>
    string? Mobile { get; set; }
    /// <summary>
    /// 拓展信息
    /// </summary>
    IDictionary<string, string>? Extras { get; set; }
}
