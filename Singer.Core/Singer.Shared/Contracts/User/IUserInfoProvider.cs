﻿using Singer.Shared.Contracts.User.Dtos;

namespace Singer.Shared.Contracts.User;

/// <summary>
/// 用户信息 提供者 接口 （系统服务依赖）
/// </summary>
public interface IUserInfoProvider
{
    /// <summary>
    /// 获取用户信息
    /// </summary>
    /// <param name="userId">用户id</param>
    Task<SysUserInfo?> GetSysUserInfoAsync(string userId);
}
