﻿
namespace Singer.Shared.Contracts.User.Dtos;

/// <summary>
/// 用户信息 （系统框架级）
/// </summary>
public class SysUserInfo
{
    /// <summary>
    /// 用户id
    /// </summary>
    public string? UserId { get; set; }
    /// <summary>
    /// 姓名
    /// </summary>
    public string? UserName { get; set; }
    /// <summary>
    /// 手机号
    /// </summary>
    public string? Mobile { get; set; }
}
