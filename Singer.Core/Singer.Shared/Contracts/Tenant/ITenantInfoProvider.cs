﻿using Singer.Shared.Contracts.Tenant.Dtos;

namespace Singer.Shared.Contracts.Tenant;

/// <summary>
/// 租户信息 提供者 接口 （系统服务依赖）
/// </summary>
public interface ITenantInfoProvider
{
    /// <summary>
    /// 获取租户信息
    /// </summary>
    /// <param name="tenantId">租户id</param>
    Task<SysTenantInfo?> GetSysTenantInfoAsync(string tenantId);
}
