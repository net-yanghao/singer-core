﻿
namespace Singer.Shared.Contracts.Tenant.Dtos;

/// <summary>
/// 租户信息 （系统框架级）
/// </summary>
public class SysTenantInfo
{
    /// <summary>
    /// 租户id
    /// </summary>
    public string? TenantId { get; set; }
    /// <summary>
    /// 租户唯一标识码
    /// </summary>
    public string? TenantCode { get; set; }
    /// <summary>
    /// 租户名
    /// </summary>
    public string? TenantName { get; set;}
}
