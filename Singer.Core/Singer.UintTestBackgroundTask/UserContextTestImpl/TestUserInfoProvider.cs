﻿
using Singer.Shared.Contracts.User;
using Singer.Shared.Contracts.User.Dtos;

namespace Singer.UintTestBackgroundJob.UserContextTestImpl;

public class TestUserInfoProvider : IUserInfoProvider
{
    public Task<SysUserInfo?> GetSysUserInfoAsync(string userId)
    {
        var userInfo = new SysUserInfo
        {
            UserId = userId,
            UserName = "测试用户001",
            Mobile = "18666666666"
        };
        return Task.FromResult(userInfo);
    }
}
