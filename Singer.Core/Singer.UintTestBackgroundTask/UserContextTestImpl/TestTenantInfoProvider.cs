﻿using Singer.Shared.Contracts.Tenant;
using Singer.Shared.Contracts.Tenant.Dtos;

namespace Singer.UintTestBackgroundJob.UserContextTestImpl;

public class TestTenantInfoProvider : ITenantInfoProvider
{
    public Task<SysTenantInfo?> GetSysTenantInfoAsync(string tenantId)
    {
        var tenantInfo = new SysTenantInfo
        {
            TenantId = tenantId,
            TenantCode = "tenant01",
            TenantName = "测试企业001"
        };
        return Task.FromResult(tenantInfo);
    }
}
