﻿using Singer.BackgroundJob.RabbitMQ.Contracts;

namespace Singer.UnitTestWeb.BackgroundJobs.JobArgs
{
    public class AuthorJobArgs : IBackgroundJobArgs
    {
        public string? JobLogId { get; set; }
        public string CurrentTenantId { get; set; } = "tenant001";

        public string CurrentUserId { get; set; } = "user001";

        public string AuthorId { get; set; } = "author001";

        public string AuthorName { get; set; } = "小明";
    }
}
