﻿using Singer.BackgroundJob.RabbitMQ.Contracts;

namespace Singer.UnitTestWeb.BackgroundJobs.JobArgs
{
    public class BookJobArgs : IBackgroundJobArgs
    {
        public string? JobLogId { get; set; }
        public string CurrentTenantId { get; set; } = "tenant001";

        public string CurrentUserId { get; set; } = "user001";

        public string BookId { get; set; } = "book001";

        public string BookName { get; set; } = "C#入门到精通";
    }
}
