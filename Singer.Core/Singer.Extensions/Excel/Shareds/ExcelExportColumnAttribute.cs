﻿/// <summary>
/// Excel导出列 字段特性
/// </summary>
[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
public class ExcelExportColumnAttribute : Attribute
{
    /// <summary>
    /// 列名
    /// </summary>
    public string ColumnName { get; set; }
    /// <summary>
    /// 列宽（字符个数）
    /// </summary>
    public int ColumnWidth { get; set; }

    /// <summary>
    /// Excel导出列 字段特性
    /// </summary>
    /// <param name="columnName">列名</param>
    /// <param name="columnWidth">列宽（字符个数）</param>
    public ExcelExportColumnAttribute(string columnName, int columnWidth = 14)
    {
        ColumnName = columnName;
        ColumnWidth = columnWidth;
    }
}