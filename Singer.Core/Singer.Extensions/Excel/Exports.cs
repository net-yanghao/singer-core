﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Reflection;

namespace Singer.Extensions.Excel;

public static partial class ExcelTools
{
    /// <summary>
    /// Excel 表格中一个字符的长度
    /// </summary>
    public const int ExcelCharLength = 256;
    /// <summary>
    /// 默认表格名
    /// </summary>
    public const string DefaultSheetName = "Sheet1";

    /// <summary>
    /// 将实体列表导出为Excel文件流 .xlsx文件
    /// </summary>
    /// <typeparam name="T">实体类型</typeparam>
    /// <param name="list">实体列表</param>
    /// <param name="showColumns">额外逻辑控制显示的字段</param>
    /// <param name="sheetName">导出的excel sheet名</param>
    public static byte[] ListToExcelAsync<T>(this List<T> list, List<string>? showColumns = null, string sheetName = DefaultSheetName)
    {
        if (list == null)
        {
            throw new ArgumentNullException(nameof(list));
        }

        // 是否通过额外的逻辑（传入的字段名）控制要导出的实体字段
        bool extControlColumns = showColumns != null && showColumns.Count > 0;
        if (extControlColumns)
        {
            showColumns = showColumns.Select(x => x.ToLower()).ToList();
        }

        // 创建工作表
        IWorkbook workbook = new XSSFWorkbook(); // .xlsx
        ISheet sheet = workbook.CreateSheet(sheetName);

        Type type = typeof(T);
        List<PropertyInfo> properties = [];

        // 标题行处理
        int rowIndex = 0;
        IRow titleRow = sheet.CreateRow(rowIndex);
        int columnIndex = 0; // 列索引
        foreach (var property in type.GetProperties())
        {
            var columnAttribute = property.GetCustomAttribute<ExcelExportColumnAttribute>();
            if (columnAttribute == null || (extControlColumns && !showColumns.Contains(property.Name.ToLower())))
            {
                continue;
            }
            properties.Add(property);
            ICell cell = titleRow.CreateCell(columnIndex);
            cell.SetCellType(CellType.String);
            cell.SetCellValue(columnAttribute.ColumnName);
            sheet.SetColumnWidth(columnIndex, columnAttribute.ColumnWidth * ExcelCharLength);
            columnIndex++;
        }

        // 数据行处理
        foreach (var item in list)
        {
            IRow row = sheet.CreateRow(++rowIndex);

            columnIndex = 0;
            foreach (var property in properties)
            {
                ICell cell = row.CreateCell(columnIndex++);
                cell.SetCellType(CellType.String);
                string value = property.GetValue(item)?.ToString() ?? "";
                cell.SetCellValue(value);
            }
        }

        // 保存文件流
        using MemoryStream stream = new MemoryStream();
        workbook.Write(stream);
        var bytes = stream.ToArray();
        return bytes;
    }
}
