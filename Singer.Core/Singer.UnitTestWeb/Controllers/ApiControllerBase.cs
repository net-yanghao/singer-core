﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Singer.Shared.UserContext;

namespace Singer.UnitTestWeb.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ApiControllerBase : ControllerBase
    {
        protected IUserContext? UserContext => GetService<IUserContextAccessor>()?.UserContext;
        
        protected IServiceProvider ServiceProvider;

        protected T GetRequiredService<T>() where T : class => ServiceProvider.GetRequiredService<T>();

        protected T? GetService<T>() where T : class => ServiceProvider.GetService<T>();

        public ApiControllerBase(IServiceProvider serviceProvider) 
        {
            ServiceProvider = serviceProvider;
        }
    }
}
