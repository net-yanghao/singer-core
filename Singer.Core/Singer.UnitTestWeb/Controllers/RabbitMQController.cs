﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RabbitMQ.Client;
using Singer.Middleware.RabbitMQ;
using System.Text;

namespace Singer.UnitTestWeb.Controllers;

public class RabbitMQController : ApiControllerBase
{
    IRabbitMQConnectionPool _rabbitMQConnectionPool;
    public RabbitMQController(IServiceProvider serviceProvider, IRabbitMQConnectionPool rabbitMQConnectionPool) : base(serviceProvider)
    {
        _rabbitMQConnectionPool = rabbitMQConnectionPool;
    }

    [HttpGet]
    public string Declare(string exchangeName = "ex_test1", string queueName = "queue_test1", string routingKey = "rkey_test1")
    {
        using var channel = _rabbitMQConnectionPool.Get().CreateModel();
        channel.ExchangeDeclare(exchangeName, ExchangeType.Direct, true, false);
        channel.QueueDeclare(queue: queueName, durable: true, exclusive: false, autoDelete: false);
        channel.QueueBind(queueName, exchangeName, routingKey);
        return "OK";
    }


    [HttpGet]
    public string SendMessage(string message, string exchangeName = "ex_test1", string routingKey = "rkey_test1")
    {
        using var channel = _rabbitMQConnectionPool.Get().CreateModel();
        channel.BasicPublish(exchangeName, routingKey, null, Encoding.UTF8.GetBytes(message));
        return "OK";
    }
}
