﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Singer.Framework.LocalEventBus;
using Singer.UnitTestWeb.Application.Users;
using Singer.UnitTestWeb.Shared.Events;

namespace Singer.UnitTestWeb.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    [AllowAnonymous]
    public class EventTestController(
        ILocalEventBus eventBus,
        IUserService userService
        ) : ControllerBase
    {
        [HttpPost]
        public Task Test(UserTestEventArgs args)
        {
            var users = userService.GetUsers();
            return eventBus.PublishAsync(this, args);
        }
    }
}
