﻿using Singer.Middleware.FreeSql;
using Singer.Middleware.JwtAuth;
using Singer.Middleware.Redis;
using Singer.Framework;
using Singer.UnitTestWeb.Models;
using Singer.UnitTestWeb.Infrastructure.EFCore;
using Singer.Middleware.EFCore;
using Singer.Framework.SeedData;
using Singer.UnitTestWeb.Infrastructure.FreeSql;
using Singer.Framework.CoreDI;
using Singer.Framework.LocalEventBus;
using Singer.UnitTestWeb.Domain.Users.Events;
using Singer.Middleware.RabbitMQ;
using Singer.BackgroundJob.RabbitMQ;
using Singer.UintTestBackgroundJob.JobExecutors;

namespace Singer.UnitTestWeb
{
    public static class DIExtensions
    {
        public static void AddAllServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddCores();
            services.AddRabbitMQ(configuration);
            services.AddFreeSql(configuration);
            services.AddJwtAuth(configuration);
            services.AddRedis(configuration);
            services.AddEFCore<TestEFCoreDbContext>(configuration);

            services.AddAutoMapper(typeof(TestMapperProfile));
            services.AddCoreDIServices(typeof(UserFreeSqlRepository).Assembly);
            services.AddDbSeedData<InitDbSeedData>();

            services.AddHttpContextAccessor();
            services.AddCoreDIServices(typeof(DIExtensions).Assembly);
            services.AddLocalEventBus(typeof(UserTest1EventHandler).Assembly);
            services.AddRabbitMQBackgroundJob(configuration, typeof(AuthorJobExecutor).Assembly);
        }
    }
}
