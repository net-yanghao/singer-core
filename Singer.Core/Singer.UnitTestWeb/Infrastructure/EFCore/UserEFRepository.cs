﻿using Singer.Framework.CoreDI;
using Singer.Middleware.EFCore;
using Singer.UnitTestWeb.Domain.Users;

namespace Singer.UnitTestWeb.Infrastructure.EFCore
{
    [CoreDI(typeof(IUserEFRepository))]
    public class UserEFRepository : BaseRepository<TestEFCoreDbContext, User>, IUserEFRepository
    {
        public UserEFRepository(TestEFCoreDbContext dbContext) : base(dbContext)
        {

        }
    }
}
