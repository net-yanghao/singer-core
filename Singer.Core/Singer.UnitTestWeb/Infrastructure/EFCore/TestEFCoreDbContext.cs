﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Singer.Framework.CoreDI;
using Singer.Middleware.EFCore;
using Singer.UnitTestWeb.Domain.Tests;
using Singer.UnitTestWeb.Domain.Users;

namespace Singer.UnitTestWeb.Infrastructure.EFCore
{
    public class TestEFCoreDbContext : EFCoreDbContext
    {
        public TestEFCoreDbContext(IServiceProvider serviceProvider, DbContextOptions options) : base(serviceProvider, options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Test> Tests { get; set; }
    }
}
