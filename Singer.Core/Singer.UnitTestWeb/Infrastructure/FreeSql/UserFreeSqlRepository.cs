﻿using Singer.Framework.CoreDI;
using Singer.Middleware.FreeSql;
using Singer.Shared.UserContext;
using Singer.UnitTestWeb.Domain.Users;
using System.Linq.Expressions;

namespace Singer.UnitTestWeb.Infrastructure.FreeSql
{
    // [CoreDI(typeof(IUserFreeSqlRepository))]
    public class UserFreeSqlRepository : BaseRepository<User>, IUserFreeSqlRepository
    {
        public UserFreeSqlRepository(IFreeSql fsql, IUserContextAccessor userContextAccessor) : base(fsql, userContextAccessor)
        {
        }
    }
}
