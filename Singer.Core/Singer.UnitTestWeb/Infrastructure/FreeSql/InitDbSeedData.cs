﻿using Singer.Framework.SeedData;
using Singer.UnitTestWeb.Domain.Users;
using Singer.UnitTestWeb.Models.Dtos;

namespace Singer.UnitTestWeb.Infrastructure.FreeSql
{
    public class InitDbSeedData : IDbSeedData
    {
        IFreeSql _freeSql;
        public InitDbSeedData(IFreeSql freeSql)
        {
            _freeSql = freeSql;
        }

        public async Task ExecuteAsync()
        {
            await InitUser();
        }


        private async Task InitUser()
        {
            if (!await _freeSql.Select<User>().AnyAsync())
            {
                UserDto dto = new UserDto()
                {
                    Account = "admin",
                    Age = 99,
                    Name = "管理员",
                    Password = "666"
                };
                User user = new User(dto.Account, dto.Name, dto.Age, dto.Password);
                await _freeSql.Insert(user).ExecuteAffrowsAsync();
            }
        }
    }
}
