﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Singer.UnitTestWeb.Domain.Tests
{
    [Table("Test")]
    public class Test
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
    }
}
