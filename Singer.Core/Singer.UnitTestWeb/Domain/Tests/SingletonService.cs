﻿using Singer.Framework.CoreDI;

namespace Singer.UnitTestWeb.Domain.Tests
{
    [CoreDI(null, ServiceLifetime.Singleton)]
    public class SingletonService
    {
        public void Test()
        {
            Console.WriteLine("单例服务");
        }
    }
}
