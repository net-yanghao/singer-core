﻿using Singer.Framework.CoreDI;

namespace Singer.UnitTestWeb.Domain.Tests
{
    [CoreDI(null, ServiceLifetime.Transient)]
    public class TransientService
    {
        public void Test()
        {
            Console.WriteLine("瞬时服务");
        }
    }
}
