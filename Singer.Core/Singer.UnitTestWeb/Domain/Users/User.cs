﻿using Singer.Core;
using Singer.Shared.Domain;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Singer.UnitTestWeb.Domain.Users
{
    [Table("tb_userinfo")]
    public class User : BaseEntity<long>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override long Id { get; set; }
        public string Name { get; set; }
        public string Account { get; set; }
        public string Mobile { get; set; }
        public int Age { get; set; }
        public string Password { get; set; } = "666";

        public User() { }

        public User(string account, string name, int age, string pwd)
        {
            Account = account;
            Name = name;
            Age = age;
            Password = pwd;
        }
    }
}
