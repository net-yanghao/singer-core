﻿using Singer.Core;
using Singer.Framework.LocalEventBus;
using Singer.UnitTestWeb.Application;
using Singer.UnitTestWeb.Shared.Events;
using Singer.UnitTestWeb.Domain.Tests;

namespace Singer.UnitTestWeb.Domain.Users.Events
{
    [LocalEventHandler(typeof(UserTestEventArgs), 1)]
    public class UserTest1EventHandler(SingletonService singletonService, ScopeService scopeService, TransientService transientService) : ILocalEventHandler<UserTestEventArgs>
    {
        public Task OnEventHandlerAsync(object sender, UserTestEventArgs e)
        {
            Console.WriteLine($"事件1被'{sender.GetType().Name}'触发，参数：" + JsonUtils.ToJson(e));
            try
            {
                singletonService.Test();
                scopeService.Test();
                transientService.Test();
            }
            catch (Exception ex)
            {
            }
            return Task.CompletedTask;
        }
    }
}
