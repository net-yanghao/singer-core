﻿using Newtonsoft.Json;
using Singer.Core;
using Singer.Framework.LocalEventBus;
using Singer.Shared.Contracts.User;
using Singer.UnitTestWeb.Shared.Events;

namespace Singer.UnitTestWeb.Domain.Users.Events
{
    [LocalEventHandler(typeof(UserTestEventArgs), 2)]
    public class UserTest2EventHandler(IUserInfoProvider userInfoProvider) : ILocalEventHandler<UserTestEventArgs>
    {
        public async Task OnEventHandlerAsync(object sender, UserTestEventArgs e)
        {
            var user = await userInfoProvider.GetSysUserInfoAsync(e.UserId);
            Console.WriteLine($"事件2被'{sender.GetType().Name}'触发，" +
                $"参数：'{JsonUtils.ToJson(e)}'，" +
                $"执行获取的用户信息：'{JsonConvert.SerializeObject(user)}'");
        }
    }
}
