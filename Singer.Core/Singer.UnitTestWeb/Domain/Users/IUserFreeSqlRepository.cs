﻿using Singer.Middleware.FreeSql;

namespace Singer.UnitTestWeb.Domain.Users
{
    public interface IUserFreeSqlRepository : IBaseRepository<User>
    {

    }
}
