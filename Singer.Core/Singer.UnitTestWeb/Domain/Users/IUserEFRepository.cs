﻿using Singer.Middleware.EFCore;
using Singer.UnitTestWeb.Infrastructure.EFCore;

namespace Singer.UnitTestWeb.Domain.Users
{
    public interface IUserEFRepository : IBaseRepository<TestEFCoreDbContext, User>
    {
    }
}
