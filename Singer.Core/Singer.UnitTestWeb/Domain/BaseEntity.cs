﻿using Singer.Shared.Domain;
using System.ComponentModel.DataAnnotations.Schema;

namespace Singer.UnitTestWeb.Domain;

/// <summary>
/// 实体基类
/// </summary>
public class BaseEntity<T> : IEntity<T>, ICreator, IChanger, ISoftDelete
{
    public virtual T Id { get; set; }
    [Column(TypeName = "varchar(32)")]
    public string? CreatorId { get; protected set; }
    public string? CreatorName { get; protected set; }
    public DateTime CreateTime { get; protected set; }
    [Column(TypeName = "varchar(32)")]
    public string? ChangerId { get; protected set; }
    public string? ChangerName { get; protected set; }
    public DateTime? ChangeTime { get; protected set; }
    public bool IsDeleted { get; protected set; }
    public string? DeleterId { get; protected set; }
    public string? DeleterName { get; protected set; }
    public DateTime? DeleteTime { get; protected set; }
}
