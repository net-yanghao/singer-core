﻿using AutoMapper;
using Singer.Framework.CoreDI;
using Singer.Shared.Contracts.User;
using Singer.Shared.Contracts.User.Dtos;
using Singer.UnitTestWeb.Domain.Users;
using Singer.UnitTestWeb.Models.Dtos;
using System.Collections.Concurrent;

namespace Singer.UnitTestWeb.Application.Users
{
    [CoreDI]
    [CoreDI(typeof(IUserService))]
    [CoreDI(typeof(IUserInfoProvider))]
    public class UserService(IMapper mapper) : IUserInfoProvider, IUserService
    {
        private ConcurrentBag<User> CacheUsers =
            [
                new User
                {
                    Id = 1,
                    Name = "小A",
                    Account = "aaa",
                    Mobile = "18000000001",
                    Password = "666",
                    Age = 10,
                },
                new User
                {
                    Id = 2,
                    Name = "小B",
                    Account = "bbb",
                    Mobile = "18000000002",
                    Password = "666",
                    Age = 20,
                },
                new User
                {
                    Id = 3,
                    Name = "小C",
                    Account = "ccc",
                    Mobile = "18000000003",
                    Password = "666",
                    Age = 30,
                },
                new User
                {
                    Id = 4,
                    Name = "小D",
                    Account = "ddd",
                    Mobile = "18000000004",
                    Password = "666",
                    Age = 40,
                }
            ];

        public Task<SysUserInfo?> GetSysUserInfoAsync(string userId)
        {
            var user = CacheUsers.FirstOrDefault(x => x.Id.ToString() == userId);
            var vo = new SysUserInfo
            {
                UserId = user?.Id.ToString(),
                UserName = user?.Name,
                Mobile = user?.Mobile
            };
            return Task.FromResult(vo);
        }

        public List<User> GetUsers()
        {
            return CacheUsers.ToList();
        }

        public void Test()
        {
            var user = new User
            {
                Id = 1,
                Account = "a",
                Age = 10,
                Name = "aa",
            };
            var vo = mapper.Map<UserDto>(user);
            Console.WriteLine(vo.Id + vo.Name);
        }
    }
}
