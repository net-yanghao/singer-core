﻿using Singer.UnitTestWeb.Domain.Users;

namespace Singer.UnitTestWeb.Application.Users
{
    public interface IUserService
    {
        List<User> GetUsers();
    }
}
