﻿namespace Singer.UnitTestWeb.Shared.Events
{
    public class UserTestEventArgs : EventArgs
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
    }
}
