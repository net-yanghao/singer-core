﻿using System.ComponentModel.DataAnnotations;

namespace Singer.UnitTestWeb.Models.Dtos
{
    public class UserDto
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "姓名不可为空")]
        public string Name { get; set; }
        [Range(1, 100, ErrorMessage = "年龄范围1-100")]
        public int Age { get; set; }
        [Required(ErrorMessage = "账号不可为空")]
        public string Account { get; set; }
        public string Password { get; set; }
        public string Remark { get; set; }
    }
}
