﻿using AutoMapper;
using Singer.UnitTestWeb.Domain.Users;
using Singer.UnitTestWeb.Models.Dtos;

namespace Singer.UnitTestWeb.Models
{
    public class TestMapperProfile : Profile
    {
        public TestMapperProfile() 
        {
            CreateMap<User, UserDto>()
                .ForMember(x => x.Remark, opt => opt.MapFrom(y => y.Age > 18 ? "成年人" : "未成年"))
                .ForMember(x => x.Password, opt => opt.MapFrom(y => ""));
        }
    }
}
