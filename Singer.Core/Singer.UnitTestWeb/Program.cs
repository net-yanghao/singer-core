using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.OpenApi.Models;
using Singer.BackgroundJob.RabbitMQ;
using Singer.Framework.CoreDI;
using Singer.Framework.Middlewares;
using Singer.Framework.SeedData;
using Singer.Shared.Contracts.Tenant;
using Singer.Shared.Contracts.User;
using Singer.UintTestBackgroundJob;
using Singer.UintTestBackgroundJob.Examples;
using Singer.UintTestBackgroundJob.JobExecutors;
using Singer.UintTestBackgroundJob.UserContextTestImpl;
using Singer.UnitTestWeb;
using Singer.UnitTestWeb.Controllers;
using Singer.UnitTestWeb.Domain.Users.Events;
using System.Security.Claims;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddLogging();
builder.Services.AddSignalR();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

//swagger 添加jwt验证
builder.Services.AddSwaggerGen(option =>
{
    option.AddSecurityDefinition(JwtBearerDefaults.AuthenticationScheme, new OpenApiSecurityScheme
    {
        Description = "Value: Bearer {token}",//请求时，token前面需要加上Bearer，格式：Bearer token
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey,
        Scheme = JwtBearerDefaults.AuthenticationScheme
    });
    option.AddSecurityRequirement(new OpenApiSecurityRequirement()
    {
      {
        new OpenApiSecurityScheme
        {
          Reference = new OpenApiReference
          {
            Type = ReferenceType.SecurityScheme,
            Id = JwtBearerDefaults.AuthenticationScheme
          },Scheme = "oauth2",Name = JwtBearerDefaults.AuthenticationScheme,In = ParameterLocation.Header,
        },new List<string>()
      }
    });
});

builder.Services.AddAllServices(builder.Configuration);
builder.Services.AddCoreDIServices(typeof(EventTestController).Assembly);

//跨域
builder.Services.AddCors(option =>
{
    option.AddPolicy("cors", a =>
    {
        a.AllowAnyHeader()
        .AllowAnyMethod()
        .AllowCredentials()
        .SetIsOriginAllowed(x =>
        {
            return true;
        });
    });
});

builder.Services.AddScoped<IUserInfoProvider, TestUserInfoProvider>();
builder.Services.AddScoped<ITenantInfoProvider, TestTenantInfoProvider>();

//builder.Services.AddHostedService<RabbitMQBackgroundJobExample>();
//builder.Services.AddRabbitMQBackgroundJob(builder.Configuration, typeof(AuthorJobExecutor).Assembly);

var app = builder.Build();
app.UseMiddleware<GlobalExceptionHandlerMiddleware>();
//初始化数据库种子数据
await app.InitDbSeedDataAsync();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseCors("cors");
app.UseRouting();
app.UseStaticFiles();
app.UseAuthentication();
app.UseAuthorization();
app.MapControllerRoute("default", "{controller}/{action}/{id?}");
app.Run();
