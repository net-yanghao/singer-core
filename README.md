# Singer.Core  
## .NET 8 框架项目

#### 集成.NET常用技术栈，业务项目推荐使用DDD领域驱动架构。包含以下功能/技术栈  

- DI拓展，通过特性反射，实现自动依赖注入服务；
- Redis 连接池服务；
- RabbitMQ 连接池服务；
- LocalEventBus 本地事件总线 功能；
- 基于RabbitMQ 发布订阅，实现统一后台异步托管任务框架。并实现以下强大功能：
    - 1、后台任务日志、任务运行日志 记录到数据库；
    - 2、任务消费失败后重试功能；
    - 3、任务手动取消功能；
    - 4、任务手动重启功能；
- JWT 拓展：实现token认证；基于Redis，实现创建Token、销毁Token、JWT鉴权、解析Token有效载荷并存入上下文；
- 基于JWT，实现全局用户信息上下文自动解析；
- EFCore 拓展：EFCore仓储基类、AOP审计；
- FreeSql 拓展：FreeSql 仓储拓展、AOP审计；
- 全局异常处理过滤器、WebApi统一结果处理过滤器；
- ORM种子数据拓展；
- Excel导入导出帮助类、其他常用工具类、基础数据类型拓展方法；

#### 项目结构截图
![项目结构截图](doc/Singer.Core%E9%A1%B9%E7%9B%AE%E7%BB%93%E6%9E%84%E5%9B%BE.png)

